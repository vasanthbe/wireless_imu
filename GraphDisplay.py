# telnet program example
import socket, select, string, sys
import matplotlib.pyplot as plt
from matplotlib import style
import time
from drawnow import *
import numpy as np

def makeFig(): #Create a function that makes our desired plot
    plt.ylim(-5000,5000)                                 #Set y min and max values
    plt.title('My Live Streaming Sensor Data')      #Plot the title
    plt.grid(True)                                  #Turn the grid on
    plt.ylabel('Accelaration Values')                            #Set ylabels
    plt.plot(y1data, 'ro-', label='X Acc')       #plot the temperature
    plt.legend(loc='upper left')                    #plot the legend
    plt2 = plt.twinx()                    #plot the legend
    plt2.plot(y2data, 'b^-', label='Y Acc') #plot pressure data
    plt2.ticklabel_format(useOffset=False)           #Force matplotlib to NOT autoscale y axis
    plt2.legend(loc='upper center')                    #plot the legend
    plt3 = plt.twinx()                    #plot the legend
    plt3.plot(y3data, 'go-', label='Z Acc') #plot pressure data
    plt3.ticklabel_format(useOffset=False)           #Force matplotlib to NOT autoscale y axis
    plt3.legend(loc='upper right')                    #plot the legend

#main function
if __name__ == "__main__":
	y1data = [0] * 50
	y2data = [0] * 50
	y3data = [0] * 50

	plt.ion()
	plt.title('Serial value from Arduino')
	plt.grid(True)
	plt.legend(loc='upper right')
	
	if(len(sys.argv) < 3) :
		print 'Usage : python telnet.py hostname port'
		sys.exit()
	
	host = sys.argv[1]
	port = int(sys.argv[2])
	
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.settimeout(2)
	
	# connect to remote host
	try :
		s.connect((host, port))
		plt.ion() # set plot to animated
	except :
		print 'Unable to connect'
		sys.exit()
	
	print 'Connected to remote host'
	
	while True:
		data = str(s.recv(1024))
		word = data.split('{')[1].split('}')[0]
		word = word.split(':')[1].split(',')
		y1data.append(float(word[0]))
		y2data.append(float(word[1]))
		y3data.append(float(word[2]))
		print(word)
		drawnow(makeFig)
		y1data.pop(0)
		y2data.pop(0)
		y3data.pop(0)
	s.close()
