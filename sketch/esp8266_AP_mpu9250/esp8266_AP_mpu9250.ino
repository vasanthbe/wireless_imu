/*
   Copyright (c) 2015, Majenko Technologies
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification,
   are permitted provided that the following conditions are met:

 * * Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.

 * * Redistributions in binary form must reproduce the above copyright notice, this
     list of conditions and the following disclaimer in the documentation and/or
     other materials provided with the distribution.

 * * Neither the name of Majenko Technologies nor the names of its
     contributors may be used to endorse or promote products derived from
     this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
   ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* Create a WiFi access point and provide a web server on it. */

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include "MPU9250.h"

MPU9250 mpu;

/* Set these to your desired credentials. */
const char *ssid = "ESPap";
const char *password = "ktems@123";

ESP8266WebServer server(80);

#define MAX_SRV_CLIENTS 1
WiFiServer tserver(23);
WiFiClient serverClients[MAX_SRV_CLIENTS];
/* Just a little test message.  Go to http://192.168.4.1 in a web browser
   connected to this access point to see it.
*/
void handleRoot() {
  server.send(200, "text/html", "<h1>You are connected</h1>");
}

void setup() {
  delay(1000);
  Serial.begin(115200);
  //start I2C
  Wire.begin();

  WiFi.mode(WIFI_STA);
  
  Serial.println();
  Serial.print("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.on("/", handleRoot);
  server.begin();
  Serial.println("HTTP server started");

  tserver.begin();
  tserver.setNoDelay(true);

  Serial1.print("Setting up IMU");
  delay(2000);
  mpu.setup();

  Serial1.print("Ready! Use 'telnet ");
  Serial1.print(WiFi.localIP());
  Serial1.println(" 23' to connect");
}

void loop() {
  server.handleClient();

uint8_t i;
  //check if there are any new clients
  //check if there are any new clients
  if (tserver.hasClient()) {
    for (i = 0; i < MAX_SRV_CLIENTS; i++) {
      //find free/disconnected spot
      if (!serverClients[i] || !serverClients[i].connected()) {
        if (serverClients[i]) {
          serverClients[i].stop();
        }
        serverClients[i] = tserver.available();
        Serial1.print("New client: "); Serial1.print(i);
        break;
      }
    }
    //no free/disconnected spot so reject
    if (i == MAX_SRV_CLIENTS) {
      WiFiClient serverClient = tserver.available();
      serverClient.stop();
      Serial1.println("Connection rejected ");
    }
  }

  //get IMU data and sent data to clients after specified interval of 16 msecs
  static uint32_t prev_ms = millis();
  if ((millis() - prev_ms) > 16)
  {
      String sdata;
      float *adata;
      prev_ms = millis();
      mpu.update();
      //mpu.print();
      adata = mpu.GetRawAcc();
      sdata = "{a:" + String(1000 *adata[0]) + "," + String(1000 *adata[1]) + "," + String(1000 *adata[2]) + "}";
      Serial.print(sdata);
      Serial.println(sizeof(sdata));
        
    //push UART data to all connected telnet clients
    for (i = 0; i < MAX_SRV_CLIENTS; i++) {
      if (serverClients[i] && serverClients[i].connected()) {
        //Convert String function adds 2 more bytes into data stream
        serverClients[i].write(sdata.c_str(), sizeof(sdata)*2 + 2);
      }
    }

//      mpu.printRawJSONData();
//        Serial.print("roll  (x-forward (north)) : ");
//        Serial.println(mpu.getRoll());
//        Serial.print("pitch (y-right (east))    : ");
//        Serial.println(mpu.getPitch());
//        Serial.print("yaw   (z-down (down))     : ");
//        Serial.println(mpu.getYaw());      
  }
}
