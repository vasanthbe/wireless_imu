/*
  WiFiTelnetMPU9250 - Example Transparent UART to Telnet Server for esp8266

  This file is part of the ESP8266WiFi library for Arduino environment.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
*/

#include <ESP8266WiFi.h>
#include "MPU9250.h"

MPU9250 mpu;

//how many clients should be able to telnet to this ESP8266
#define MAX_SRV_CLIENTS 1
const char* ssid = "vasanth";
const char* password = "Sachu@20Sep";

WiFiServer server(23);
WiFiClient serverClients[MAX_SRV_CLIENTS];

void setup() {
  //start UART
  Serial.begin(115200);
  //start I2C
  Wire.begin();

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("\nConnecting to "); Serial.println(ssid);
  uint8_t i = 0;
  while (WiFi.status() != WL_CONNECTED && i++ < 20) {
    delay(500);
  }
  if (i == 21) {
    Serial.print("Could not connect to"); Serial.println(ssid);
    while (1) {
      delay(500);
    }
  }
  //start UART and the server
  server.begin();
  server.setNoDelay(true);

  Serial.print("Ready! Use 'telnet ");
  Serial.print(WiFi.localIP());
  Serial.println(" 23' to connect");

  delay(2000);
  mpu.setup();
}

void loop() {
  uint8_t i;
  //check if there are any new clients
  if (server.hasClient()) {
    for (i = 0; i < MAX_SRV_CLIENTS; i++) {
      //find free/disconnected spot
      if (!serverClients[i] || !serverClients[i].connected()) {
        if (serverClients[i]) {
          serverClients[i].stop();
        }
        serverClients[i] = server.available();
        Serial.print("New client: "); Serial.print(i);
        break;
      }
    }
    //no free/disconnected spot so reject
    if (i == MAX_SRV_CLIENTS) {
      WiFiClient serverClient = server.available();
      serverClient.stop();
      Serial.println("Connection rejected ");
    }
  }
  //get IMU data and sent data to clients after specified interval of 16 msecs
  static uint32_t prev_ms = millis();
  if ((millis() - prev_ms) > 16)
  {
      String sdata;
      float *adata;
      prev_ms = millis();
      mpu.update();
      //mpu.print();
      adata = mpu.GetRawAcc();
      sdata = "{a:" + String(1000 *adata[0]) + "," + String(1000 *adata[1]) + "," + String(1000 *adata[2]) + "}";
      Serial.print(sdata);
      Serial.println(sizeof(sdata));
        
    //push UART data to all connected telnet clients
    for (i = 0; i < MAX_SRV_CLIENTS; i++) {
      if (serverClients[i] && serverClients[i].connected()) {
        //Convert String function adds 2 more bytes into data stream
        serverClients[i].write(sdata.c_str(), sizeof(sdata)*2 + 2);
      }
    }

//      mpu.printRawJSONData();
//        Serial.print("roll  (x-forward (north)) : ");
//        Serial.println(mpu.getRoll());
//        Serial.print("pitch (y-right (east))    : ");
//        Serial.println(mpu.getPitch());
//        Serial.print("yaw   (z-down (down))     : ");
//        Serial.println(mpu.getYaw());      
  }
}
